## Summary

This is my attempt on implementing a shutter release remote for Olympus cameras. The project is implemented using an Atmel ATTiny85 microcontroller. The Olympus remotes use NEC IR protocol with the address set to 0x61DC, and the command for shutter release is 0x80.

## Software

This project was written using Arduino IDE 1.8.2, SeeJayDee's [tiny_IRremote](https://gist.github.com/SeeJayDee/caa9b5cc29246df44e45b8e7d1b1cdc5) library, and SpenceKonde's [ATTiny Core](https://github.com/SpenceKonde/ATTinyCore).

Currently implemented functions are:
 - Single shutter release
 - Automatic shutter release every 5s

As there is no power switch, the software uses Attiny's Power-down mode to conserve power when not in use. I've measured the idle current to be less than 0.01mA (smallest value that my multimeter can measure). The repeat shutter mode, on the other hand, consumes about 5-6mA, therefore it should be canceled by pressing Button 1 when not used to conserve the battery.
## Hardware

TODO: add schematic diagram

The IR LED is controlled from ATTiny's Arduino Pin 4 (AVR pin PB4, ATTiny85 lead 3). I've connected 3 pushbuttons between ground and Arduino pins 0(PB0, lead 5), 1(PB1, lead 6), 2(PB2, lead 7). Currently only pins 0 and 1 are used. Pin 1 controls single shutter release and cancels repeat shutter mode. Pin 0 starts repeat shutter and is used to trigger the shutter outside of the standard interval in repeat shutter mode. The IR LED is switched by a 2N3904 transistor through a 5 Ohm resistor. The whole circuit is powered by a single li-ion mobile phone battery.