#include <tiny_IRremote.h>
#include <avr/sleep.h>

IRsend irsend;

const uint32_t address = 0x61DC;

const uint8_t shutterCommand = 0x80;

static inline void sendRepeat()
{
  const uint16_t buf[3] {9000, 2250, 560};
  irsend.sendRaw(buf, 3, 38);
  delay(95);
}

static inline void sendCommand(uint8_t command)
{
  const uint32_t value = (address << 16) | (uint16_t(command) << 8) | uint8_t(~command);
  irsend.sendNEC(value, 32);
  delay(40);
}

static inline void setupInterrupts()
{
  PCMSK |= _BV(PCINT0) | _BV(PCINT1) | _BV(PCINT2);
  GIFR |= _BV(PCIF);
  GIMSK |= _BV(PCIE);
}

static inline bool button0Pressed()
{
  return !(PINB & _BV(PB0));
}

static inline bool button1Pressed()
{
  return !(PINB & _BV(PB1));
}

static inline bool button2Pressed()
{
  return !(PINB & _BV(PB2));
}

ISR (PCINT0_vect)
{
}

static inline void setupPins()
{
  DDRB &= ~(_BV(PB0) | _BV(PB1) | _BV(PB2) | _BV(PB3));
  PORTB |= _BV(PB0) | _BV(PB1) | _BV(PB2) | _BV(PB3);
}

void setup()
{
  setupPins();
  setupInterrupts();

}

static inline void powerDown()
{
  uint8_t adcsra = ADCSRA;
  ADCSRA = 0;
  uint8_t acsr = ACSR;
  ACSR |= 0x80;
  uint8_t prr = PRR;
  PRR |= 0x07;

  // Choose our preferred sleep mode:
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);

  // Set sleep enable (SE) bit:
  sleep_enable();

  // Put the device to sleep:
  sleep_mode();

  // Upon waking up, sketch continues from this point.
  sleep_disable();
  PRR = prr;
  ACSR = acsr;
  ADCSRA = adcsra;
}

enum class RemoteMode {
  none,
  single,
  interval
};

RemoteMode remoteMode = RemoteMode::none;

const int intervalModeDelay = 5;

static inline bool interruptibleDelaySeconds(const unsigned int s) {
  if (!s) {
    return false;
  }
  const unsigned long msEnd = millis() + s * 1000UL;
  const uint8_t mask = _BV(PB0) | _BV(PB1)/* | _BV(PB2)*/; // Button 2 does not do anything for now
  delay(50);
  uint8_t previousButtons = PINB & mask;
  while(long(msEnd - millis()) > 0) {
    delay(50);
    uint8_t currentButtons = PINB & mask;
    if (previousButtons & (~PINB) & mask) {
      return true;
    }
    previousButtons = currentButtons;
  }
  return false;
}

void loop()
{
  do {
    switch (remoteMode) {
      case RemoteMode::none:
        if (button1Pressed()) {
          remoteMode = RemoteMode::single;
          sendCommand(shutterCommand);
        } else if (button0Pressed()) {
          remoteMode = RemoteMode::interval;
          interruptibleDelaySeconds(intervalModeDelay);
        }
        break;
      case RemoteMode::single:
        if (button1Pressed()) {
          sendRepeat();
        } else if (button0Pressed()) {
          remoteMode = RemoteMode::interval;
          interruptibleDelaySeconds(intervalModeDelay);
        } else {
          remoteMode = RemoteMode::none;
        }
        break;
      case RemoteMode::interval:
        if (button1Pressed()) {
          remoteMode = RemoteMode::single;
          sendCommand(shutterCommand);
        } else {
          sendCommand(shutterCommand);
          interruptibleDelaySeconds(intervalModeDelay);
        }
        break;
    }
  } while (remoteMode != RemoteMode::none);

  powerDown();
}
